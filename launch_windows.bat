@echo off
cd src
python main.py 2>debug 3>log
if errorlevel 1
    echo "Error occurred while running. Please check src/debug for more information."
    type debug | more
else
    echo "The program exited normally."
