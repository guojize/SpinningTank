#!/bin/bash
cd src
python3 main.py 2>debug 3>log
if [ $? -ne 0 ]; then
    echo "Error occurred while running. Please check src/debug for more information."
    cat debug | more
else
    echo "The program exited normally."
fi
