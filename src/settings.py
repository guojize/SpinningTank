# include settings page
# TODO: need to be restructured
# TODO: deprecate settings.json

import pygame

from options import *
from animation import *
from utils import *

def save():
    fout = open('../settings.json', 'w')
    fout.write('{\n')
    fout.write('    "cheat": {},\n'.format('true' if cheat else 'false'))
    fout.write('    "fullScreen": {},\n'.format('true' if fullScreen else 'false'))
    fout.write('    "volume": {}\n'.format(volume))
    fout.write('}\n')
    fout.close()

def fullscreen(screen):
    bgColor = random.choice(colors)
    playerPic = [None] * 4
    for i in range(4):
        playerPic[i] = pygame.image.load(playerPath(i))
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                save()
                exit()
            elif event.type == pygame.KEYDOWN and event.key in dashKey:
                dash = dashKey.index(event.key) // 3
                global latestDash
                latestDash = dash
                bulletShoot(dash ^ 2 if dash % 2 == 0 else dash)
                recoil(dash)
                if dash == 0:
                    return RETURN
                elif dash == 1:
                    return BACK
                elif dash == 2:
                    global fullScreen
                    fullScreen = not fullScreen
                    if fullScreen == True:
                        screen = pygame.display.set_mode((0, 0), pygame.FULLSCREEN)
                    else:
                        screen = pygame.display.set_mode(screenSize)
            elif event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                save()
                exit()

        pressed = False
        global frame
        if frame in animation.keys():
            for todo in animation[frame]:
                if todo[0] == PRESSBUTTON:
                    pressed = True
        pygame.draw.rect(screen, bgColor, (transform(-playSize // 2, -playSize // 2), (playSize, playSize)))
        drawPicture(screen, pygame.image.load(edgePath), transform(0, 0), (playSize, playSize))
        text(screen, 'FULLSCREEN', regularFontSize, mainTextCenter)
        text(screen, ('TRUE' if fullScreen else 'FALSE'), smallFontSize, subTextCenter)
        drawPicture(screen, pygame.image.load(menuUp), transform(0, -(screenSize[1] // 2 - menuPadding)), (50, 50))
        drawPicture(screen, pygame.image.load(menuLeft), transform(-(screenSize[1] // 2 - menuPadding), 0), (50, 50))
        drawPicture(screen, pygame.image.load(buttonPressedDown if pressed else buttonDown), transform(0, screenSize[1] // 2 - 14), (66, 28))
        drawPicture(screen, playerPic[latestDash ^ 2 if latestDash % 2 == 0 else latestDash], transform(playerPos[0], playerPos[1]), avatarSize)
        animationHandler(screen)
        pygame.display.flip()
        frame += 1

def cheatMode(screen):
    bgColor = random.choice(colors)
    playerPic = [None] * 4
    for i in range(4):
        playerPic[i] = pygame.image.load(playerPath(i))
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                save()
                exit()
            elif event.type == pygame.KEYDOWN and event.key in dashKey:
                dash = dashKey.index(event.key) // 3
                global latestDash
                latestDash = dash
                bulletShoot(dash ^ 2 if dash % 2 == 0 else dash)
                recoil(dash)
                if dash == 0:
                    return RETURN
                elif dash == 2:
                    global cheat
                    cheat = not cheat
                elif dash == 3:
                    ret = fullscreen(screen)
                    if ret == RETURN:
                        return RETURN
            elif event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                save()
                exit()

        pressed = False
        global frame
        if frame in animation.keys():
            for todo in animation[frame]:
                if todo[0] == PRESSBUTTON:
                    pressed = True
        pygame.draw.rect(screen, bgColor, (transform(-playSize // 2, -playSize // 2), (playSize, playSize)))
        drawPicture(screen, pygame.image.load(edgePath), transform(0, 0), (playSize, playSize))
        text(screen, 'CHEAT', regularFontSize, mainTextCenter)
        text(screen, ('TRUE' if cheat else 'FALSE'), smallFontSize, subTextCenter)
        drawPicture(screen, pygame.image.load(menuUp), transform(0, -(screenSize[1] // 2 - menuPadding)), (50, 50))
        drawPicture(screen, pygame.image.load(buttonPressedDown if pressed else buttonDown), transform(0, screenSize[1] // 2 - 14), (66, 28))
        drawPicture(screen, pygame.image.load(menuRight), transform(screenSize[1] // 2 - menuPadding, 0), (50, 50))
        drawPicture(screen, playerPic[latestDash ^ 2 if latestDash % 2 == 0 else latestDash], transform(playerPos[0], playerPos[1]), avatarSize)
        animationHandler(screen)
        pygame.display.flip()
        frame += 1

def settingsPage(screen):
    bgColor = random.choice(colors)
    playerPic = [None] * 4
    for i in range(4):
        playerPic[i] = pygame.image.load(playerPath(i))
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                save()
                exit()
            elif event.type == pygame.KEYDOWN and event.key in dashKey:
                dash = dashKey.index(event.key) // 3
                global latestDash
                latestDash = dash
                bulletShoot(dash ^ 2 if dash % 2 == 0 else dash)
                recoil(dash)
                if dash == 3:
                    save()
                    return BACK
                elif dash == 2:
                    cheatMode(screen)
            elif event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                save()
                exit()
        pygame.draw.rect(screen, bgColor, (transform(-playSize // 2, -playSize // 2), (playSize, playSize)))
        drawPicture(screen, pygame.image.load(edgePath), transform(0, 0), (playSize, playSize))
        text(screen, 'SETTINGS', regularFontSize, mainTextCenter)
        drawPicture(screen, pygame.image.load(menuDown), transform(0, screenSize[1] // 2 - menuPadding), (50, 50))
        drawPicture(screen, pygame.image.load(menuRight), transform(screenSize[1] // 2 - menuPadding, 0), (50, 50))
        animationHandler(screen)
        drawPicture(screen, playerPic[latestDash ^ 2 if latestDash % 2 == 0 else latestDash], transform(playerPos[0], playerPos[1]), avatarSize)
        pygame.display.flip()
        global frame
        frame += 1
