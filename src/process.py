# include level parsing and processing

import pickle
import json

def jsonLoad(path):
    with open(path, 'r') as fin:
        jsonStr = '\n'.join(fin.readlines())
    return json.loads(jsonStr)

# the class containing level data
class Level:
    def __init__(self, src): # init from JSON file
        raw = jsonLoad(src)
        self.levelName = raw['level.name']
        self.artist = raw['music.artist']
        self.coverPath = raw['music.coverPath']
        self.difficulty = raw['level.difficulty']
        self.musicPath = raw['level.musicPath']
        self.musicTime = raw['timeline.musicTime']
        self.speed = raw['timeline.speed'] * 1000  # save in ms
        self.hitNum = raw['timeline.hitNum']
        self.timeline = list(map(lambda x: x * 10, raw['timeline.melody']))  # save in ms
        self.friends = raw['timeline.friends']

    def save(self, savePath):  # save to pickle binary file
        with open(savePath, 'wb') as fout:
            pickle.dump(self, fout, 2)

def load(savePath):  # load from pickle binary file
    with open(savePath, 'rb') as fin:
        return pickle.load(fin)

'''
for fn in ('Ringtone','CauseYouAreSoPretty','Lemon','China_C','AlwaysWithMe','Calorie','SongofCrash','TrippyLove'):
    Level('../projects/LevelProject/%s.json' % fn).save('../assets/level/%s' % fn)
'''
