# include utility functions

import pygame
import json

from options import *

def textDraw(screen, text, size, padding=0, up=0, down=0):
    space = screenSize[1] - up - down
    font = pygame.font.Font(fontPath, size)
    y = -(len(text) - 1) * (size + padding) / 2 + up
    for line in text:
        pic = font.render(line, False, (255, 255, 255))
        rect = pic.get_rect()
        rect.center = transform(0, y)
        screen.blit(pic, rect)
        y += (size + padding)

def drawPicture(screen, pic, pos, size):
    image = pygame.transform.scale(pic, size)
    rect = image.get_rect()
    rect.center = pos
    screen.blit(image, rect)

def transform(x, y):
    return (x + screenSize[0] // 2, y + screenSize[1] // 2)

def text(screen, text, size, pos, color=(0, 0, 0)):
    font = pygame.font.Font(fontPath, size)
    pic = font.render(text, False, color)
    rect = pic.get_rect()
    rect.center = pos
    screen.blit(pic, rect)
