# include core gaming stuff
# TODO: need to be restructured
# TODO: fix (possibly) perfectHit feature
# TODO: fix early quitting

import pygame
import threading
from time import perf_counter as timer
from time import sleep

from options import *
from queue_ import queue
from utils import *
from animation import *

backgroundPic = pygame.image.load(edgePath)
HPPic, enemyPic, friendPic, playerPic = [None] * 7, [None] * 5, [None] * 8, [None] * 4
for i in range(1, 7):
    HPPic[i] = pygame.image.load(HPPath(i))
for i in range(1, 5):
    enemyPic[i] = pygame.image.load(enemyPath(i))
for i in range(8):
    friendPic[i] = pygame.image.load(friendPath(i))
for i in range(4):
    playerPic[i] = pygame.image.load(playerPath(i))
regularText = pygame.font.Font(fontPath, regularFontSize)
bigText = pygame.font.Font(fontPath, bigFontSize)

HP = 4
combo = 0

def HPChange(val):
    global HP
    HP = (min(HP + val, 6) if not cheat else 6)
    if val < 0:
        if v2:
            global combo
            combo = 0
        pass

def mainLoop(screen, currentLevel):
    timeline = currentLevel.timeline
    timeline.append(19260817)
    speed = currentLevel.speed
    friends = currentLevel.friends
    objHP = [4] * currentLevel.hitNum
    indexes = {}
    for obj in friends:
        objHP[obj] = 1
        indexes[obj] = random.randint(0, 7)
    onDash = [random.randint(0, 3) for _ in range(currentLevel.hitNum)] # TODO
    queued = [queue() for _ in range(4)]
    global frame
    global latestDash
    cnt = -1
    finished = 0
    AP = FC = fullHP = True
    clock = pygame.time.Clock()
    start = timer()
    while True:
        shoot = 0
        reach = 0
        killed = 0
        now = int((timer() - start) * 1000)
        if now >= timeline[cnt + 1]:
            cnt += 1
            queued[onDash[cnt]].push(cnt)
        global combo
        for q in queued:
            if not q.empty():
                top = q.front()
                if now - timeline[top] >= speed:
                    q.pop()
                    finished += 1
                    if top in friends:
                        if v2: combo += 1
                        HPChange(1)
                        bgChange(colors[indexes[top]])
                    else:
                        HPChange(-2)
                        AP = fullHP = FC = False
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN and event.key in dashKey:
                shoot += 1
                dash = dashKey.index(event.key) // 3
                if dash % 2 == 0: dash ^= 2 # mogic
                latestDash = dash
                bulletShoot(dash)
                if not queued[dash].empty():
                    obj = queued[dash].front()
                    pos = (now - timeline[obj]) / speed
                    bulletReach(dash, pos)
                    reach += 1
                    if pos >= perfectHit: objHP[obj] -= 4
                    else:
                        objHP[obj] -= 2
                        AP = False
                    if objHP[obj] <= 0:
                        queued[dash].pop()
                        finished += 1
                        killed += 1
                        enemyDisappear(direction[dash](pos))
                        if obj in friends:
                            HPChange(-2)
                            AP = fullHP = FC = False
            elif event.type == pygame.QUIT:
                exit()
            elif event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                return QUITINLEVEL, (False, False, False, False)
        if shoot == reach:
            combo += killed
        else:
            combo = 0
            AP = FC = False
        if finished == currentLevel.hitNum: return LEVELFINISHED, (AP, fullHP, False, FC)
        if HP <= 0: return LEVELFAILED, (False, False, finished <= 10, False)

        if shoot > 0: recoil(latestDash)
        if frame in animation.keys():
            for todo in animation[frame]:
                if todo[0] == CHANGEBGCOLOR:
                    global bgColor
                    bgColor = todo[1]
        pygame.draw.rect(screen, bgColor, (transform(-playSize // 2, -playSize // 2), (playSize, playSize)))
        animationHandler(screen)
        drawPicture(screen, backgroundPic, transform(0, 0), (playSize, playSize))
        for i in range(4):
            if not queued[i].empty():
                for obj in queued[i].arr[queued[i].head:queued[i].index]:
                    ratio = (now - timeline[obj]) / speed
                    pos = direction[i](ratio)
                    pic = friendPic[indexes[obj]] if obj in friends else enemyPic[objHP[obj]]
                    drawPicture(screen, pic, pos, avatarSize) # TODO
        drawPicture(screen, playerPic[latestDash], transform(playerPos[0], playerPos[1]), avatarSize)
        if not v2 or combo >= minCombo:
            text = bigText if combo and combo % comboPerHighlight == 0 else regularText
            comboPic = text.render('x%d' % combo, False, (0, 0, 0))
            rect = comboPic.get_rect()
            rect.left, rect.bottom = 0 + comboPadding[0], screenSize[1] - comboPadding[1]
            screen.blit(comboPic, rect)
        percentPic = regularText.render('%d%%' % int(now / (currentLevel.musicTime * 1000) * 100), False, (0, 0, 0))
        rect = percentPic.get_rect()
        rect.right, rect.top = screenSize[0] - comboPadding[0], 0 + comboPadding[1]
        screen.blit(percentPic, rect)
        rect = HPPic[HP].get_rect()
        rect.bottom, rect.right = screenSize[0] - HPPadding[0], screenSize[1] - HPPadding[1]
        screen.blit(HPPic[HP], rect)
        pygame.display.flip()
        frame += 1
        clock.tick(maxFPS)

def playMusic(currentLevel):
    sleep(perfectHit * currentLevel.speed / 1000)
    pygame.mixer.music.load(currentLevel.musicPath)
    pygame.mixer.music.set_volume(volume / 100)
    pygame.mixer.music.play()

def levelClear(screen, state):
    screen.fill((0, 0, 0))
    pygame.draw.rect(screen, bgColor, (transform(-playSize // 2, -playSize // 2), (playSize, playSize)))
    regularFont = pygame.font.Font(fontPath, regularFontSize)
    pic = regularFont.render('LEVEL CLEARED', False, (0, 0, 0))
    rect = pic.get_rect()
    rect.center = mainTextCenter
    screen.blit(pic, rect)
    msg = ''
    if cheat: msg = 'BUT CHEATED'
    elif state[0]: msg = 'AND ALL PERFECT'
    elif state[3]: msg = 'AND FULL COMBO'
    elif state[1]: msg = 'AND NO HP LOSING'
    else: msg = "THAT'S ALL"
    smallFont = pygame.font.Font(fontPath, smallFontSize)
    pic2 = smallFont.render(msg, False, (0, 0, 0))
    rect2 = pic2.get_rect()
    rect2.center = subTextCenter
    screen.blit(pic2, rect2)
    pygame.display.flip()

def levelFail(screen):
    screen.fill((0, 0, 0))
    pygame.draw.rect(screen, bgColor, (transform(-playSize // 2, -playSize // 2), (playSize, playSize)))
    regularFont = pygame.font.Font(fontPath, regularFontSize)
    pic = regularFont.render('LEVEL FAILED', False, (0, 0, 0))
    rect = pic.get_rect()
    rect.center = mainTextCenter
    screen.blit(pic, rect)
    smallFont = pygame.font.Font(fontPath, smallFontSize)
    pic2 = smallFont.render('TRY IT AGAIN', False, (0, 0, 0))
    rect2 = pic2.get_rect()
    rect2.center = subTextCenter
    screen.blit(pic2, rect2)
    pygame.display.flip()

def startLevel(screen, currentLevel):
    global frame, animation, playerPos, HP, combo, bgColor
    frame = 0
    animation = {}
    playerPos = (0, 0)
    HP = (6 if cheat else 4)
    combo = 0
    bgColor = random.choice(colors)

    musicTime = currentLevel.musicTime
    speed = currentLevel.speed
    timeline = currentLevel.timeline

    pygame.draw.rect(screen, bgColor, (transform(-playSize // 2, -playSize // 2), (playSize, playSize)))
    drawPicture(screen, backgroundPic, transform(0, 0), (playSize, playSize))
    drawPicture(screen, playerPic[0], transform(0, 0), avatarSize)
    if not v2:
        comboPic = regularText.render('x0', False, (0, 0, 0))
        rect = comboPic.get_rect()
        rect.left, rect.bottom = 0 + comboPadding[0], screenSize[1] - comboPadding[1]
        screen.blit(comboPic, rect)
    rect = HPPic[HP].get_rect()
    rect.bottom, rect.right = screenSize[0] - HPPadding[0], screenSize[1] - HPPadding[1]
    screen.blit(HPPic[HP], rect)
    pygame.display.flip()

    pygame.mixer.Sound(startLevelPath).play()
    sleep(3)
    pygame.mixer.Sound(beepPath).play()
    music = threading.Thread(target=playMusic, args=(currentLevel,))
    music.start()
    ret, state = mainLoop(screen, currentLevel)
    pygame.mixer.quit()
    pygame.mixer.init()

    sleep(1)
    if ret == LEVELFINISHED: levelClear(screen, state)
    elif ret == LEVELFAILED: levelFail(screen)
    sleep(2)
    pygame.event.get()
    if cheat: state = (False, False, False, False)
    return ret, state
