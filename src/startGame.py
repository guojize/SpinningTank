# include startup page

import pygame

from options import *
from utils import *

def startup(screen):
    pygame.display.set_caption(gameTitle)
    startupPic = pygame.image.load(startupPicturePath)
    while True:
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    exit()
                else:
                    return GAMESTARTED
            elif event.type == pygame.QUIT:
                exit()
        drawPicture(screen, startupPic, transform(0, 0), (playSize, playSize))
        pygame.display.flip()
