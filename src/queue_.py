# include a queue to replace the FUCKING queue in python standard lib.

class queue:
    def __init__(self, size=1024):
        self.size = size
        self.arr = [None] * size
        self.index = 0
        self.head = 0
        self.len = 0

    def empty(self):
        return self.len == 0

    def front(self):
        return self.arr[self.head]

    def push(self, val):
        self.len += 1
        self.arr[self.index] = val
        self.index = (0 if self.index + 1 == self.size else self.index + 1)

    def pop(self):
        self.len -= 1
        self.head += 1
