# include gaming constants
# TODO: fix colors
# plan: add achievements

import pygame
import random

from process import jsonLoad

err = pygame.init()[1]
if err != 0:
    raise ImportError('pygame load failed')

# settings
settingsPath = '../settings.json'
settings = jsonLoad(settingsPath)
cheat = settings['cheat']
volume = settings['volume']
fullScreen = settings['fullScreen']

# metadata
version = 'v0.1.0-dev-20191002'
gameTitle = "Spinnin' Tank"
maxFPS = 60
windowSize = (600, 600)
screenSize = (pygame.display.Info().current_w, pygame.display.Info().current_h) if fullScreen else windowSize
playSize = screenSize[1]

# path
startupPicturePath = '../assets/picture/startup_2.png'
iconPath = '../assets/picture/icon.png'
levelIndexPath = '../assets/level/info.json'
fontPath = '../assets/fonts/04B_03B.ttf'
startLevelPath = '../assets/soundtrack/CountDownIn3Sec.ogg'
beepPath = '../assets/soundtrack/Start.ogg'
edgePath = '../assets/picture/background/background_1.png'
bulletShoot1 = '../assets/picture/animation/bullet_shoot1.png'
bulletShoot2 = '../assets/picture/animation/bullet_shoot2.png'
bulletReach1 = '../assets/picture/animation/bullet_hit1.png'
bulletReach2 = '../assets/picture/animation/bullet_hit2.png'
menuUp = '../assets/picture/menu/array_up.png'
menuLeft = '../assets/picture/menu/array_left.png'
menuRight = '../assets/picture/menu/array_right.png'
menuDown = '../assets/picture/menu/array_down.png'
buttonUp = '../assets/picture/button/button_up_popped.png'
buttonDown = '../assets/picture/button/button_down_popped.png'
textBackground = '../assets/picture/menu/textBackground.png'

def HPPath(HP): return '../assets/picture/animation/HP' + str(HP) + '.png'
def enemyPath(HP): return '../assets/picture/animation/enemy_HP' + str(HP) + '.png'
def friendPath(index): return '../assets/picture/avatar/friend' + str(index) + '.png'
def playerPath(dash): return '../assets/picture/animation/player' + str(dash) + '.png'

bulletShoot1 = '../assets/picture/animation/bullet_shoot1.png'
bulletShoot2 = '../assets/picture/animation/bullet_shoot2.png'
bulletReach1 = '../assets/picture/animation/bullet_hit1.png'
bulletReach2 = '../assets/picture/animation/bullet_hit2.png'
arrayUp = '../assets/picture/menu/array_up.png'
arrayLeft = '../assets/picture/menu/array_left.png'
arrayRight = '../assets/picture/menu/array_right.png'
arrayDown = '../assets/picture/menu/array_down.png'
buttonUp = '../assets/picture/button/button_up_popped.png'
buttonPressedUp = '../assets/picture/button/button_up_pressed.png'
buttonDown = '../assets/picture/button/button_down_popped.png'
buttonPressedDown = '../assets/picture/button/button_down_pressed.png'

levelStr = ('AlwaysWithMe', 'China_C', 'SongofCrash', 'TrippyLove', 'Calorie', 'CauseYouAreSoPretty', 'Ringtone', 'Lemon')
artistStr = ('YUMI KIMURA', 'XU MENGYUAN', 'DJ TITON', 'VENXENTO', 'ROCKET GIRLS 101', 'CAI XUKUN (SWIN)', 'METROGNOME', 'HACHI')
titleStr = ('ALWAYS WITH ME', 'CHINA-C', 'SONG OF CRASH', 'TRIPPY LOVE', 'CALORIE', "YOU'RE SO PRETTY", 'RINGTONE', 'LEMON')

coverPath = backgroundPath = {}
coverPath[1]      = '../assets/cover/AlwaysWithMe.jpg'
backgroundPath[1] = '../assets/cover/DimAlwaysWithMe.jpg'
coverPath[2]      = '../assets/cover/China_C.jpg'
backgroundPath[2] = '../assets/cover/DimChina_C.jpg'
coverPath[3]      = '../assets/cover/SongofCrash.jpg'
backgroundPath[3] = '../assets/cover/DimSongofCrash.jpg'
coverPath[4]      = '../assets/cover/TrippyLove.jpg'
backgroundPath[4] = '../assets/cover/DimTrippyLove.jpg'
coverPath[5]      = '../assets/cover/Calorie.jpg'
backgroundPath[5] = '../assets/cover/DimCalorie.jpg'
coverPath[6]      = '../assets/cover/CauseYouAreSoPretty.jpg'
backgroundPath[6] = '../assets/cover/DimCauseYouAreSoPretty.jpg'
coverPath[7]      = '../assets/cover/Ringtone.jpg'
backgroundPath[7] = '../assets/cover/DimRingtone.jpg'
coverPath[8]      = '../assets/cover/Lemon.jpg'
backgroundPath[8] = '../assets/cover/DimLemon.jpg'

def artist(val):
    return artistStr[val - 1]

def title(val):
    return titleStr[val - 1]

def level_(val):
    return levelStr[val - 1]

# design
mainTextCenter = (screenSize[0] // 2, screenSize[1] // 2 - 100)
subTextCenter = (screenSize[0] // 2, screenSize[1] // 2 + 75)
HPPadding = (5, 30)
comboPadding = (50, 40)
avatarSize = (80, 80)
playerPadding = avatarSize[0]
dashLength = (playSize - playerPadding) // 2
colors = ((0xf6, 0xb3, 0x3f), (0xac, 0xd5, 0x98), (0x7e, 0xce, 0xf4), (0x88, 0xab, 0xda),
          (0x8c, 0x97, 0xcb), (0xc4, 0x90, 0xbf), (0xf2, 0x97, 0x9f), (0xf6, 0xb3, 0x7f))
bgChangeFrames = maxFPS // 3
recoilLength = avatarSize[0] // 10
recoilFrames = maxFPS // 5
bulletFrame = maxFPS // 20
enemyDisappearFrame = maxFPS // 15
butoonPressFrame = maxFPS // 15
smallFontSize = 48
regularFontSize = int(64.0)
bigFontSize = 80
creditsPadding = 10
creditsIconSize = 50
settingsPadding = 16
menuPadding = 30

# logic
perfectHit = 0.4  # 40% ~ 100% is perfect
dashKey = (pygame.K_w, pygame.K_i, pygame.K_UP,
           pygame.K_a, pygame.K_j, pygame.K_LEFT,
           pygame.K_s, pygame.K_k, pygame.K_DOWN,
           pygame.K_d, pygame.K_l, pygame.K_RIGHT)
minCombo = 3
comboPerHighlight = 10
v2 = True

# constants
GAMESTARTED = 0
LEVELFINISHED = 1
LEVELFAILED = 2
QUITINLEVEL = 3
COLOREDSQUARE = 4
CHANGEBGCOLOR = 5
GOTO = 6
BULLETSHOOT = 7
BULLETREACH = 8
PRESSBUTTON = 9
BACK = 10
RETURN = 11
HOME = 12
CREDITS = 13
SETTINGS = 14
ACHIEVEMENTS = 15
LEVELS = 16

# others
creditsText = \
(
    "SPINNIN' TANK",
    '',
    'MADE BY',
    'ALLEN, CHARLES',
    'COPYRIGHT (c) 2019',
    '',
    'Allen20050107',
)

#achievements = \
#(
#    'All perfect',
#    'Are you sure?',
#    'Enjoy the video',
#    'Full combo',
#    'Hidden',
#    'The first one',
#    'The last one',
#)
