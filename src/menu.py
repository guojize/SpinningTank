# include main menu and level selection
# TODO: need to be restructured

import pygame

from options import *
from utils import *
from animation import *

def home(screen):
    bgColor = random.choice(colors)
    playerPic = [None] * 4
    for i in range(4):
        playerPic[i] = pygame.image.load(playerPath(i))
    clock = pygame.time.Clock()
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                exit()
            elif event.type == pygame.KEYDOWN and event.key in dashKey:
                dash = dashKey.index(event.key) // 3
                global latestDash
                latestDash = dash
                bulletShoot(dash ^ 2 if dash % 2 == 0 else dash)
                recoil(dash)
                if dash == 0:
                    return CREDITS
                elif dash == 1:
                    return SETTINGS
                elif dash == 2:
                    return ACHIEVEMENTS
                elif dash == 3:
                    return LEVELS
        pygame.draw.rect(screen, bgColor, (transform(-playSize // 2, -playSize // 2), (playSize, playSize)))
        drawPicture(screen, pygame.image.load(edgePath), transform(0, 0), (playSize, playSize))
        text(screen, "SPINNIN' TANK", regularFontSize, mainTextCenter)
        drawPicture(screen, pygame.image.load(arrayDown), transform(0, screenSize[1] // 2 - menuPadding), (50, 50))
        drawPicture(screen, pygame.image.load(arrayRight), transform(screenSize[1] // 2 - menuPadding, 0), (50, 50))
        drawPicture(screen, pygame.image.load(arrayLeft), transform(menuPadding - screenSize[1] // 2, 0), (50, 50))
        drawPicture(screen, pygame.image.load(arrayUp), transform(0, menuPadding - screenSize[1] // 2), (50, 50))
        animationHandler(screen)
        drawPicture(screen, playerPic[latestDash ^ 2 if latestDash % 2 == 0 else latestDash], transform(
            playerPos[0], playerPos[1]), avatarSize)
        pygame.display.flip()
        clock.tick(maxFPS)
        global frame
        frame += 1

def menuPage(screen):
    bgColor = random.choice(colors)
    playerPic = [None] * 4
    for i in range(4):
        playerPic[i] = pygame.image.load(playerPath(i))
    clock = pygame.time.Clock()
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                exit()
            elif event.type == pygame.KEYDOWN and event.key in dashKey:
                dash = dashKey.index(event.key) // 3
                global latestDash
                latestDash = dash
                bulletShoot(dash ^ 2 if dash % 2 == 0 else dash)
                recoil(dash)
                if dash == 1:
                    return HOME
                elif dash == 2:
                    return menuSelection(screen)
        pygame.draw.rect(screen, bgColor, (transform(-playSize // 2, -playSize // 2), (playSize, playSize)))
        drawPicture(screen, pygame.image.load(edgePath), transform(0, 0), (playSize, playSize))
        text(screen, 'LEVELS', regularFontSize, mainTextCenter)
        drawPicture(screen, pygame.image.load(arrayDown), transform(0, screenSize[1] // 2 - menuPadding), (50, 50))
        drawPicture(screen, pygame.image.load(arrayLeft), transform(menuPadding - screenSize[1] // 2, 0), (50, 50))
        animationHandler(screen)
        drawPicture(screen, playerPic[latestDash ^ 2 if latestDash % 2 == 0 else latestDash], transform(
            playerPos[0], playerPos[1]), avatarSize)
        pygame.display.flip()
        clock.tick(maxFPS)
        global frame
        frame += 1

def menuSelection(screen):
    playerPic = [None] * 4
    curLevel = 1
    for i in range(4):
        playerPic[i] = pygame.image.load(playerPath(i))
    clock = pygame.time.Clock()
    while True:
        curMenuPath = pygame.image.load(coverPath[curLevel])
        curBackgroundPath = pygame.image.load(backgroundPath[curLevel])
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                exit()
            elif event.type == pygame.KEYDOWN and event.key in dashKey:
                dash = dashKey.index(event.key) // 3
                global latestDash
                latestDash = dash
                bulletShoot(dash ^ 2 if dash % 2 == 0 else dash)
                recoil(dash)
                if dash == 0:
                    return BACK
                elif dash == 1:
                    curLevel = max(1, curLevel - 1)
                elif dash == 2:
                    return curLevel
                elif dash == 3:
                    curLevel = min(8, curLevel + 1)
        screen.fill((0, 0, 0))
        drawPicture(screen, pygame.image.load(edgePath), transform(0, 0), (playSize, playSize))
        drawPicture(screen, curBackgroundPath, transform(0, 0), (playSize, playSize))
        bg = pygame.image.load(textBackground)
        screen.blit(bg, bg.get_rect())
        drawPicture(screen, curMenuPath, transform(0, 175), (150, 150))
        text(screen, title(curLevel), regularFontSize, mainTextCenter)
        text(screen, artist(curLevel), smallFontSize, transform(0, 60))
        drawPicture(screen, pygame.image.load(arrayDown), transform(0, screenSize[1] // 2 - menuPadding), (50, 50))
        drawPicture(screen, pygame.image.load(arrayRight), transform(screenSize[1] // 2 - menuPadding, 0), (50, 50))
        drawPicture(screen, pygame.image.load(arrayLeft), transform(menuPadding - screenSize[1] // 2, 0), (50, 50))
        drawPicture(screen, pygame.image.load(arrayUp), transform(0, menuPadding - screenSize[1] // 2), (50, 50))
        animationHandler(screen)
        drawPicture(screen, playerPic[latestDash ^ 2 if latestDash % 2 == 0 else latestDash], transform(
            playerPos[0], playerPos[1]), avatarSize)
        pygame.display.flip()
        clock.tick(maxFPS)
        global frame
        frame += 1
