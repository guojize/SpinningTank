from time import perf_counter as timer
from pygame import mixer
from sys import argv

mixer.init()
mixer.music.load(argv[1] if len(argv) == 2 else 'res.ogg')

input('Press enter to start')
mixer.music.play()
start = timer()

fout = open('hits.txt', 'w')
while True:
    input()
    fout.write(str(int((timer() - start) * 100)))
    fout.write(' ')
    fout.flush()
