# include animation stuff
# plan: process PRESSBUTTON

import pygame
import random

from options import *
from utils import *

def up(x): return transform(0, playSize // 2 - x * dashLength)
def left(x): return transform(x * dashLength - playSize // 2, 0)
def down(x): return transform(0, x * dashLength - playSize // 2)
def right(x): return transform(playSize // 2 - x * dashLength, 0)
direction = (up, left, down, right)

frame = 0
latestDash = 0
animation = {}
playerPos = (0, 0)
bgColor = random.choice(colors)

def tryAppend(pos, thing):
    if pos in animation.keys():
        animation[pos].append(thing)
    else:
        animation[pos] = [thing]

def bgChange(rgb):
    for i in range(bgChangeFrames):
        thing = (COLOREDSQUARE, rgb, transform(0, 0), i / bgChangeFrames * playSize)
        tryAppend(frame + i, thing)
    tryAppend(frame + bgChangeFrames, (CHANGEBGCOLOR, rgb))

def recoil(dash):
    x, y = ((0, -recoilLength), (recoilLength, 0), (0, recoilLength), (-recoilLength, 0))[dash]
    periodFrames = recoilFrames // 2
    for i in range(periodFrames):
        tryAppend(frame + i, (GOTO, i / periodFrames * x, i / periodFrames * y))
    for i in range(periodFrames):
        tryAppend(frame + periodFrames + i, (GOTO, (periodFrames - i) / periodFrames * x, (periodFrames - i) / periodFrames * y))

def bulletShoot(dash):
    shoot1 = pygame.image.load(bulletShoot1)
    shoot1 = pygame.transform.rotate(shoot1, dash * 90)
    for i in range(bulletFrame):
        tryAppend(frame + i, (BULLETSHOOT, shoot1, dash))

    shoot2 = pygame.image.load(bulletShoot2)
    shoot2 = pygame.transform.rotate(shoot2, dash * 90)
    for i in range(bulletFrame):
        tryAppend(frame + bulletFrame + i, (BULLETSHOOT, shoot2, dash))

def bulletReach(dash, pos):
    reach1 = pygame.image.load(bulletReach1)
    reach1 = pygame.transform.rotate(reach1, (dash ^ 2 if dash % 2 == 0 else dash) * 90) # mogic
    for i in range(bulletFrame):
        tryAppend(frame + i, (BULLETREACH, reach1, dash, direction[dash](pos)))

    reach2 = pygame.image.load(bulletReach2)
    reach2 = pygame.transform.rotate(reach2, (dash ^ 2 if dash % 2 == 0 else dash) * 90) # mogic
    for i in range(bulletFrame):
        tryAppend(frame + bulletFrame + i, (BULLETREACH, reach2, dash, direction[dash](pos)))

def enemyDisappear(pos):
    for i in range(enemyDisappearFrame):
        tryAppend(frame + i, (COLOREDSQUARE, (0, 0, 0), pos, (enemyDisappearFrame - i) / enemyDisappearFrame * avatarSize[0] // 2))

def pressButton(dash):
    for i in range(bulletFrame):
        tryAppend(frame + i, (PRESSBUTTON, dash))

def animationHandler(screen):
    if frame in animation.keys():
        for todo in animation[frame]:
            if todo[0] == COLOREDSQUARE:
                pygame.draw.rect(screen, todo[1], (-todo[3] // 2 + todo[2][0], -todo[3] // 2 + todo[2][1], todo[3], todo[3]))
            elif todo[0] == GOTO:
                global playerPos
                playerPos = (todo[1], todo[2])
            elif todo[0] == BULLETSHOOT:
                tmpA = transform(playerPadding // 2, 0)
                tmpB = transform(-playerPadding // 2, 0)
                rect = todo[1].get_rect()
                if todo[2] == 0: rect.top, rect.centerx = tmpA
                elif todo[2] == 1: rect.right, rect.centery = tmpB
                elif todo[2] == 2: rect.bottom, rect.centerx = tmpB
                elif todo[2] == 3: rect.left, rect.centery = tmpA
                screen.blit(todo[1], rect)
            elif todo[0] == BULLETREACH:
                rect = todo[1].get_rect()
                if todo[2] == 0: rect.top, rect.centerx = (todo[3][1] - playerPadding // 2, todo[3][0])
                elif todo[2] == 1: rect.left, rect.centery = (todo[3][0] + playerPadding // 2, todo[3][1])
                elif todo[2] == 2: rect.bottom, rect.centerx = (todo[3][1] + playerPadding // 2, todo[3][0])
                elif todo[2] == 3: rect.right, rect.centery = (todo[3][0] - playerPadding // 2, todo[3][1])
                screen.blit(todo[1], rect)
        animation.pop(frame)
