# include credits page

import pygame

from options import *
from utils import *

def credits(screen):
    screen.fill((0, 0, 0))
    textDraw(screen, creditsText, smallFontSize, creditsPadding, up=60)
    icon = pygame.image.load(iconPath)
    icon = pygame.transform.scale(icon, (creditsIconSize * 2, creditsIconSize * 2))
    rect = icon.get_rect()
    rect.centerx = screenSize[0] // 2
    rect.top = 80 - creditsIconSize
    screen.blit(icon, rect)
    pygame.display.flip()
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                exit()
            elif event.type == pygame.KEYDOWN and event.key in dashKey:
                dash = dashKey.index(event.key) // 3
                if dash == 2: return BACK
