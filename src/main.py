# include entry point
# plan: finish achievementsHallway

import pygame

import level
import process
import startGame
from credits_ import credits
from menu import home, menuPage
from options import *
from process import Level
from settings import settingsPage

if fullScreen == True:
    screen = pygame.display.set_mode((0, 0), pygame.FULLSCREEN)
else:
    screen = pygame.display.set_mode(windowSize)

startGame.startup(screen)
while True:
    ret = home(screen)
    if ret == CREDITS:
        credits(screen)
    elif ret == SETTINGS:
        settingsPage(screen)
    elif ret == ACHIEVEMENTS:
        #achievementsHallway(screen)
        exit()
    elif ret == LEVELS:
        while True:
            status = menuPage(screen)
            if status == HOME: break
            elif status != BACK:
                level.startLevel(screen, process.load('../assets/level/' + level_(status)))
